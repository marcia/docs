## GitLab Marketing Team - Developer Relations

### Technical Writers - Documentation

This project tracks all issues related to:

- Existing documentation: needs update or complete refactor
- New docs: ask for new docs

### Useful links:

- Documentation: https://docs.gitlab.com/
  - GitLab CE docs:https://docs.gitlab.com/ce/
  - GitLab EE and GitLab.com docs: https://docs.gitlab.com/ee/
  - Gitlab CI docs: https://docs.gitlab.com/ce/ci/
  - Omnibus GitLab docs: https://docs.gitlab.com/ce/ci/

- Documentation project: https://gitlab.com/gitlab-com/doc-gitlab-com

- Handbook:
  - [Marketing](https://about.gitlab.com/handbook/marketing/)
  - [Tech Writing](https://about.gitlab.com/handbook/marketing/developer-relations/technical-writing/)
  - [Markdown Guide](https://about.gitlab.com/handbook/marketing/developer-relations/technical-writing/markdown-guide/) for about.GitLab.com

### Tech Writing Team

- [Axil](https://gitlab.com/u/axil)
- [Marcia](https://gitlab.com/u/marcia)
- [Sean Packham](https://gitlab.com/u/seanpackham)
